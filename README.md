# Sources [Origin System]

## Background
There are some publishers, that have multiple databases or systems with differents type of process or different type of data in each one, generating a different release with different fields in each case. For the user point of view it could be much easier if the release itself has the information about where data it comes from.

## Extension fields

This extension adds a `sources` field to at `release` level in which the publisher can publish from which system(s) the data in the release come fromand introduces a new `Source` building block.

The `sources` field is an array of `Source` building blocks.

The `Source` building block is made up of three fields:

* `id` - A local identifier for this source. This field may be built with the following structure {ocid prefix}-{sample-source}

* `name` - The name of the origin system the data in the release come.

* `url` - A web address for to the system.

## Example

The following JSON snippet models a contracting process where sources are applicable.

```json
{
	...
	"releases": [
		{
		"sources": [
			{
				"id": "ocds-213czf-sample-source",
				"name": "Sample Source",
				"url": "http://example.com"
			},
			{
				"id": "ocds-213czf-honducompras",
				"name": "HonduCompras 1.0",
				"url": "http://h1.honducompras.gob.hn/"
			}
		],
		"ocid": "ocds-213czf-000-00001",
		"id": "ocds-213czf-000-00001-05-contract",
		"date": "2010-05-10T10:30:00Z",
		"language": "es",
		...
		}
	]
}
```

## Usage notes

There are some cases where the data source for a release comes from different information systems of the same publisher.

There are some cases in which you can link the data from the contracts database with the bidding and traditional process databases.
